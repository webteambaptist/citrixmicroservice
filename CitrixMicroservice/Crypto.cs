﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using NLog;
namespace CitrixMicroservice
{
    public class Crypto
    {
        private static string PHash;
        private static string SaltKey;
        private static string VIKey;
        private readonly IConfiguration _config;
        public Crypto(IConfiguration config)
        {
            _config = config;
            PHash = _config.GetSection("AppSettings").GetSection("FURL").Value;
            SaltKey = _config.GetSection("AppSettings").GetSection("URL").Value;
            VIKey = _config.GetSection("AppSettings").GetSection("Crypto").GetSection("VIKey").Value;
            var nLogConfig = new NLog.Config.LoggingConfiguration();
            var loggerFile = new NLog.Targets.FileTarget("Logfile")
                {FileName = $"Logs\\CitrixMicroservice-{DateTime.Now:MM-dd-yyyy}.log"};
            nLogConfig.AddRule(LogLevel.Info, LogLevel.Fatal, loggerFile);
            NLog.LogManager.Configuration = nLogConfig;
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string Decrypt(string encTxtIn)
        {
            string encTxt = Base64Decode(encTxtIn); //decode first, then decrypt
            byte[] cipherTextBytes = Convert.FromBase64String(encTxt);
            byte[] keyBytes = new Rfc2898DeriveBytes(PHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());

        }

        public static string Encrypt(string pTxt)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(pTxt);

            byte[] keyBytes = new Rfc2898DeriveBytes(PHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Base64Encode(Convert.ToBase64String(cipherTextBytes));

        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
