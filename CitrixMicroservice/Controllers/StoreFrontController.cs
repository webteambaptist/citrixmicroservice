﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using CitrixMicroservice.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using NLog;
using RestSharp;
using LogLevel = NLog.LogLevel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CitrixMicroservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreFrontController : ControllerBase
    {
        private readonly IConfiguration _config;
        private static string BaseSfurl;
        private static string FileSfurl;
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private PhysiciansPortalContext ppEntities;
        public StoreFrontController(IConfiguration config, PhysiciansPortalContext context)
        {
            _config = config;
            ppEntities = context;
            FileSfurl = _config.GetSection("AppSettings").GetSection("FURL").Value;
            BaseSfurl = _config.GetSection("AppSettings").GetSection("URL").Value;
            var nLogConfig = new NLog.Config.LoggingConfiguration();
            var loggerFile = new NLog.Targets.FileTarget("Logfile")
                {FileName = $"Logs\\CitrixMicroservice-{DateTime.Now:MM-dd-yyyy}.log"};
            nLogConfig.AddRule(LogLevel.Info, LogLevel.Fatal, loggerFile);
            NLog.LogManager.Configuration = nLogConfig;
        }

        // GET: api/<StoreFrontController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] {"value1", "value2"};
        }

        [Route("authenticate")]
        [HttpGet]
        public ActionResult authenticate()
        {
            Logger.Info("authenticate STARTED");
            var headers = Request.Headers;

            try
            {
                if (headers.ContainsKey("scuser"))
                {
                    //assume BH is the domain
                    //get cookie, split at ':', decrypt password
                    var cookie = headers["scuser"].First(); //get from header
                    var cookiePieces = cookie.Split(':');
                    if (cookiePieces.Length > 1)
                    {
                        //always use SSL!
                        var returnValues = new Dictionary<string, string>();
                        var csrfToken = Guid.NewGuid().ToString();
                        var aspnetSessionId = Guid.NewGuid().ToString();
                        var username = cookiePieces[0];
                        var pwd = cookiePieces[1].ToString();
                        var password = Crypto.Base64Decode(pwd);
                        // string _password = Crypto.Decrypt(cookiePieces[1]); //cookiePieces[1];
                        const string domain = "bh";
                        var sfurl = "https://" + BaseSfurl;
                        Logger.Info("SFURL:" + sfurl);
                        if (username.StartsWith("ad\\", true, null))
                        {
                            username = username.Substring(3);
                        }

                        var authenticationBody = string.Format("username={0}\\{1}&password={2}", domain, username,
                            HttpUtility.UrlEncode(password));
                        Logger.Info("username is " + username);
                        var _rc = new RestClient(sfurl);
                        Logger.Info("SFURL:" + sfurl);

                        var authReq = new RestRequest("/PostCredentialsAuth/Login", Method.POST);
                        authReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
                        authReq.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                        authReq.AddHeader("Csrf-Token", csrfToken);
                        authReq.AddCookie("csrftoken", csrfToken);
                        authReq.AddCookie("asp.net_sessionid", aspnetSessionId);
                        authReq.AddParameter("application/x-www-form-urlencoded", authenticationBody,
                            ParameterType.RequestBody);

                        var authResp = _rc.Execute(authReq);
                        Logger.Info("Checking for headers where Name == Set-Cookie...");
                        foreach (var header in authResp.Headers.Where(i => i.Name == "Set-Cookie"))
                        {
                            Logger.Info(header.Name);
                            Console.WriteLine(header.Name);
                            Logger.Info("Checking for cookieValues...");
                            var cookieValues = header.Value?.ToString()?.Split(',');

                            if (cookieValues == null) continue;
                            foreach (var cookieValue in cookieValues)
                            {
                                Logger.Info("Adding cookieValue = " + cookieValue);
                                try
                                {
                                    var cookieElements = cookieValue.Split(';');
                                    var keyValueElements = cookieElements[0].Split('=');
                                    returnValues.Add(keyValueElements[0].Trim(), keyValueElements[1].Trim()); //1
                                }
                                catch (Exception ex)
                                {
                                    Logger.Info("StorefrontController/authenticate(), ERROR adding cookieValue",
                                        ex.Message);
                                }
                            }
                        }

                        Logger.Info("authenticate FINISHED");
                        Logger.Info("_returnvalues: " + returnValues);
                        return Ok(returnValues);
                    }

                    Logger.Info(
                        "StorefrontController/authenticate(); AUTHENTICATION FAILED: CookiePieces.Length is NOT > 1. CookiePieces.Length = " +
                        cookiePieces.Length);
                    return Ok("Missing Data");
                }
                else
                {
                    Logger.Info(
                        "StorefrontController/authenticate(); AUTHENTICATION FAILED: HEADER does NOT contain scuser.");
                    return Ok("Missing Data");
                }
            }
            catch (Exception ex)
            {
                Logger.Info("authenticate FAILED");
                Logger.Info("api/sf/authenticate");
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// get list of user specific apps
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        [Route("getlist")]
        [HttpPost]
        public ActionResult GetList([FromBody] ResourceModel res)
        {
            if (res == null) //check headers
            {
                try
                {
                    var headers = Request.Headers;

                    if (headers != null)
                    {
                        res = new ResourceModel
                        {
                            SessionID = headers["SessionID"].First(),
                            CsrfToken = headers["CsrfToken"].First(),
                            AuthID = headers["CtxsAuthId"].First()
                        };

                    }
                }
                catch (Exception ex)
                {
                    return BadRequest("Missing Headers");
                }
            }

            try
            {
                Logger.Info("getList STARTED");
                var applicationList = new List<CitrixApplicationInfo>();

                var sfurl = "https://" + BaseSfurl;

                var rc = new RestClient(sfurl);
                var getResourcesReq = new RestRequest(@"Resources/List", Method.POST);
                Logger.Info("SFURL:" + sfurl);
                Logger.Info("Adding headerds to _getResourcesReq...");
                getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
                Logger.Info("X-Citrix-IsUsingHTTPS header ADDED");
                getResourcesReq.AddHeader("Accept", "application/json");
                //Logger.Info("Accept header ADDED");
                getResourcesReq.AddHeader("Csrf-Token", res.CsrfToken.Trim());
                // Logger.Info("Csrf-Token header ADDED");
                getResourcesReq.AddCookie("csrftoken", res.CsrfToken.Trim());
                //Logger.Info("csrftoken header ADDED");
                getResourcesReq.AddCookie("asp.net_sessionid", res.SessionID.Trim());
                //Logger.Info("asp.net_sessionid header ADDED");
                getResourcesReq.AddCookie("CtxsAuthId", res.AuthID.Trim());
                //Logger.Info("CtxsAuthId header ADDED");
                getResourcesReq.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                var resourceListResp = rc.Execute(getResourcesReq);

                var json = resourceListResp.Content;

                var a = JObject.Parse(json);
                var resources = (JArray) a["resources"];

                var apps = ppEntities.AppImageMatrices;
                var images = ppEntities.Images;

                Logger.Info("Checking resources...");

                if (resources != null)
                    foreach (var o in resources)
                    {
                        Logger.Info("Checking resource: " + o);
                        var r = new CitrixApplicationInfo();

                        try
                        {
                            r.AppDesc = o["description"]?.ToString();
                        }
                        catch (Exception e)
                        {
                            r.AppDesc = "";
                        }

                        //new code to handle external AppIcons
                        //do match on
                        //load entity, find where name == name in DB, load URL into r.appicon https://ws.bmcjax.com/Storefront/
                        var name = o["name"].ToString();

                        try
                        {
                            r.AppOrder = 9;
                            try
                            {
                                var matrix = apps.First(x => x.AppName == name);
                                var path = images.First(y => y.Id == matrix.ImageId);

                                try
                                {
                                    r.AppOrder = matrix.AppOrder;
                                }
                                catch
                                {
                                    r.AppOrder = 9;
                                }

                                if (path != null)
                                {
                                    r.AppIcon = FileSfurl + path.FileName;
                                }
                                else
                                {
                                    r.AppIcon = FileSfurl + "ie.png";
                                }

                                r.AppOrder = matrix.AppOrder;
                            }
                            catch (Exception e)
                            {
                                r.AppIcon = FileSfurl + "ie.png";
                            }

                            r.AppIcon = r.AppIcon.Replace("\r\n", "");
                            r.AppLaunchURL = o["launchurl"].ToString();
                            r.ID = o["id"].ToString();
                            r.AppTitle = o["name"].ToString();
                            var nameContents = o["name"].ToString().ToUpper();
                            applicationList.Add(r);
                        }
                        catch (Exception)
                        {
                            r.AppIcon = FileSfurl + "ie.png";
                        }
                    }

                var outList = applicationList.OrderBy(x => x.AppOrder).ThenBy(x => x.AppTitle);

                return Ok(outList);
            }
            catch (Exception ex)
            {
                Logger.Info("getList FAILED");
                Logger.Info("api/sf/getList" + ex.InnerException.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// get ica file
        /// </summary>
        [Route("getICA")]
        [HttpPost]
        public string LaunchApplication([FromBody] ResourceModel res)
        {
            Logger.Info("getIca STARTED");

            if (res == null) //check headers
            {
                try
                {
                    var headers = Request.Headers;

                    if (headers != null)
                    {
                        res = new ResourceModel
                        {
                            SessionID = headers["SessionID"].First(),
                            CsrfToken = headers["CsrfToken"].First(),
                            AuthID = headers["CtxsAuthId"].First(),
                            AppID = headers["AppID"].First()
                        };
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Exception getting headers from getICA :: " + ex.Message);
                    return null;
                }
            }

            try
            {
                Logger.Info("getIca, sessionID:" + res.SessionID + " authID:" + res.AuthID + " csrfToken:" +
                            res.CsrfToken + " appID:" + res.AppID);
                var result = GetIcaString(res.SessionID, res.AuthID, res.CsrfToken, res.AppID, true);
                Console.WriteLine(result);
                Logger.Info("result = " + result);               

                return result;
            }
            catch (Exception ex)
            {
                Logger.Info("getIca FAILED");
                Logger.Info("api/sf/getIca", ex.Message);
               
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="authId"></param>
        /// <param name="csrfToken"></param>
        /// <param name="appId"></param>
        /// <param name="useSsl"></param>
        /// <returns></returns>
        public static string GetIcaString(string sessionId, string authId, string csrfToken, string appId, bool useSsl)
        {
            try
            {
                string sfurl = null;

                if (useSsl)
                {
                    sfurl = "https://" + BaseSfurl;
                }
                else
                {
                    sfurl = "http://" + BaseSfurl;
                }

                Logger.Info("SFURL:" + sfurl);
                var rc = new RestClient(sfurl);

                var getResourcesReq = new RestRequest(appId, Method.GET);

                getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", useSsl ? "Yes" : "No");
                getResourcesReq.AddHeader("Content-Type", "application/octet-stream");
                getResourcesReq.AddHeader("Csrf-Token", csrfToken);
                getResourcesReq.AddCookie("csrftoken", csrfToken);
                getResourcesReq.AddCookie("asp.net_sessionid", sessionId);
                getResourcesReq.AddCookie("CtxsAuthId", authId);
                Logger.Info("getIcaString, sessionID:" + sessionId + " authID:" + authId + " csrfToken:" + csrfToken +
                            " appID:" + appId);
                var resourceListResp = rc.Execute(getResourcesReq);

                var icaFileString = resourceListResp.Content;
                Logger.Info("getIcaString, _icaFileString:" + icaFileString);
                return icaFileString;
            }
            catch (Exception ex)
            {
                Logger.Info("StorefrontController/getIcaString", ex.InnerException?.Message);
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="AuthID"></param>
        /// <param name="CsrfToken"></param>
        /// <param name="AppID"></param>
        /// <param name="UseSSL"></param>
        /// <returns></returns>
        public static string GetIcaStringAsJSON(string SessionID, string AuthID, string CsrfToken, string AppID, bool UseSSL)
        {
            try
            {
                string SFURL = null;

                if (UseSSL)
                {
                    SFURL = "https://" + BaseSfurl;
                }
                else
                {
                    SFURL = "http://" + BaseSfurl;
                }
                Logger.Info("SFURL:" + SFURL);
                RestClient _rc = new RestClient(SFURL);
                //RestRequest _getResourcesReq = new RestRequest(string.Format("Resources/LaunchIca/{0}", AppID), Method.GET);

                RestRequest _getResourcesReq = new RestRequest(AppID, Method.GET);

                if (UseSSL)
                {
                    _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
                }
                else
                {
                    _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
                }
                _getResourcesReq.AddHeader("Content-Type", "application/json");
                _getResourcesReq.AddHeader("Csrf-Token", CsrfToken);
                _getResourcesReq.AddCookie("csrftoken", CsrfToken);
                _getResourcesReq.AddCookie("asp.net_sessionid", SessionID);
                _getResourcesReq.AddCookie("CtxsAuthId", AuthID);
                IRestResponse _resourceListResp = _rc.Execute(_getResourcesReq);

                string _icaFileString = _resourceListResp.Content; // .Replace("\r","");

                return _icaFileString;// .Replace("\n","");
            }
            catch (Exception ex)
            {
                Logger.Info("StorefrontController/GetIcaStringAsJSON", ex);
                return null;
            }
        }
    }
}
