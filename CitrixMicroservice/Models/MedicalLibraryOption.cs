﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class MedicalLibraryOption
    {
        public int Id { get; set; }
        public string DropDownName { get; set; }
    }
}
