﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class ScheduleHtml
    {
        public int Id { get; set; }
        public string Schedulename { get; set; }
        public string Html { get; set; }
        public DateTime Updatedate { get; set; }
    }
}
