﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class PhysiciansPortalContext : DbContext
    {
        public PhysiciansPortalContext()
        {
        }

        public PhysiciansPortalContext(DbContextOptions<PhysiciansPortalContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AppImageMatrix> AppImageMatrices { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<GeneralPhysicianInfo> GeneralPhysicianInfos { get; set; }
        public virtual DbSet<Hospital> Hospitals { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<MedicalLibraryOption> MedicalLibraryOptions { get; set; }
        public virtual DbSet<MedicalLibraryOptionValue> MedicalLibraryOptionValues { get; set; }
        public virtual DbSet<PhysicianInfoHub> PhysicianInfoHubs { get; set; }
        public virtual DbSet<PhysicianQuicklink> PhysicianQuicklinks { get; set; }
        public virtual DbSet<PhysicianTaskList> PhysicianTaskLists { get; set; }
        public virtual DbSet<PortalLog> PortalLogs { get; set; }
        public virtual DbSet<PpdailyCheck> PpdailyChecks { get; set; }
        public virtual DbSet<ScheduleHtml> ScheduleHtmls { get; set; }
        public virtual DbSet<SocialFeedAttachment> SocialFeedAttachments { get; set; }
        public virtual DbSet<SocialFeedMain> SocialFeedMains { get; set; }
        public virtual DbSet<SocialFeedMatrix> SocialFeedMatrices { get; set; }

         protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AppImageMatrix>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("AppImageMatrix", "dbo");

                entity.Property(e => e.AppName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AppOrder).HasDefaultValueSql("((9))");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.ImageId).HasColumnName("ImageID");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.ToTable("Department", "dbo");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("ID");

                entity.Property(e => e.Department1)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("Department");

                entity.Property(e => e.Link).IsRequired();

                entity.HasOne(d => d.Facility)
                    .WithMany(p => p.Departments)
                    .HasForeignKey(d => d.FacilityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Department_Hospital");
            });

            modelBuilder.Entity<GeneralPhysicianInfo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("GeneralPhysicianInfo", "dbo");

                entity.Property(e => e.Credentials)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Echoid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("ECHOID");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Hospital>(entity =>
            {
                entity.ToTable("Hospital", "dbo");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("ID");

                entity.Property(e => e.Facility)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<Image>(entity =>
            {
                entity.ToTable("Images", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ImageName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MedicalLibraryOption>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MedicalLibraryOptions", "dbo");

                entity.Property(e => e.DropDownName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");
            });

            modelBuilder.Entity<MedicalLibraryOptionValue>(entity =>
            {
                entity.ToTable("MedicalLibraryOptionValues", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Value).IsRequired();
            });

            modelBuilder.Entity<PhysicianInfoHub>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("PhysicianInfoHub", "dbo");

                entity.Property(e => e.ContentId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("ContentID");

                entity.Property(e => e.Echoid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("ECHOID");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.ReadDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<PhysicianQuicklink>(entity =>
            {
                entity.ToTable("PhysicianQuicklinks", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Echoid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("ECHOID");

                entity.Property(e => e.LinkTitle)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.LinkUrl)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("LinkURL");
            });

            modelBuilder.Entity<PhysicianTaskList>(entity =>
            {
                entity.ToTable("PhysicianTaskList", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DoctorName).HasMaxLength(255);

                entity.Property(e => e.DueDate).HasColumnType("datetime");

                entity.Property(e => e.EchoDoctorNumber).HasMaxLength(255);

                entity.Property(e => e.SubmitDate).HasColumnType("datetime");

                entity.Property(e => e.Task).HasMaxLength(255);

                entity.Property(e => e.UpdatedBy).HasMaxLength(255);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<PortalLog>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("PortalLog", "dbo");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.LastPortalLaunch)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Ntid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("NTID");
            });

            modelBuilder.Entity<PpdailyCheck>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("PPDailyCheck", "dbo");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.LastUpdateDate).HasColumnType("datetime");

                entity.Property(e => e.Ntid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("NTID")
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<ScheduleHtml>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ScheduleHtml", "dbo");

                entity.Property(e => e.Html)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("html");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Schedulename)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("schedulename");

                entity.Property(e => e.Updatedate)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedate");
            });

            modelBuilder.Entity<SocialFeedAttachment>(entity =>
            {
                entity.ToTable("SocialFeedAttachments", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDt)
                    .HasColumnType("datetime")
                    .HasColumnName("CreateDT")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PostFileName).HasMaxLength(255);

                entity.Property(e => e.PostId).HasColumnName("PostID");

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.SocialFeedAttachments)
                    .HasForeignKey(d => d.PostId)
                    .HasConstraintName("FK_SocialFeedAttachments_SocialFeedAttachments");
            });

            modelBuilder.Entity<SocialFeedMain>(entity =>
            {
                entity.HasKey(e => e.PostId);

                entity.ToTable("SocialFeedMain", "dbo");

                entity.Property(e => e.PostId).HasColumnName("PostID");

                entity.Property(e => e.CreatedDt)
                    .HasColumnType("datetime")
                    .HasColumnName("CreatedDT")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Echoid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("ECHOID");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LastUpdateDt)
                    .HasColumnType("datetime")
                    .HasColumnName("LastUpdateDT")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PostAuthor)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.PostAuthorEchoId)
                    .HasMaxLength(50)
                    .HasColumnName("PostAuthorEchoID");

                entity.Property(e => e.PostDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<SocialFeedMatrix>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("SocialFeedMatrix", "dbo");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.SubscribersPhysicianId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("SubscribersPhysicianID");

                entity.Property(e => e.SubscriptionPhysicianId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("SubscriptionPhysicianID");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
