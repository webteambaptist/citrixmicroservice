﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class PhysicianTaskList
    {
        public int Id { get; set; }
        public string EchoDoctorNumber { get; set; }
        public string Task { get; set; }
        public string DoctorName { get; set; }
        public DateTime? DueDate { get; set; }
        public bool? IsComplete { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public bool? IsSubmitted { get; set; }
        public DateTime? SubmitDate { get; set; }
    }
}
