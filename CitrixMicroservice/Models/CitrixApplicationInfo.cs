﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CitrixMicroservice.Models
{
    public class CitrixApplicationInfo
    {
        public string ID { get; set; }
        public String AppTitle { get; set; }
        public String AppLaunchURL { get; set; }
        public String AppIcon { get; set; }
        public String AppDesc { get; set; }
        public int AppOrder { get; set; }
    }
}
