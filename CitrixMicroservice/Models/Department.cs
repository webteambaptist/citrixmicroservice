﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class Department
    {
        public int Id { get; set; }
        public int FacilityId { get; set; }
        public string Department1 { get; set; }
        public string Link { get; set; }

        public virtual Hospital Facility { get; set; }
    }
}
