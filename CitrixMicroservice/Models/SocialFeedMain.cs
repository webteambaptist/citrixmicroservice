﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class SocialFeedMain
    {
        public SocialFeedMain()
        {
            SocialFeedAttachments = new HashSet<SocialFeedAttachment>();
        }

        public int Id { get; set; }
        public int PostId { get; set; }
        public string PostAuthor { get; set; }
        public DateTime PostDate { get; set; }
        public string PostBody { get; set; }
        public DateTime CreatedDt { get; set; }
        public DateTime LastUpdateDt { get; set; }
        public string Echoid { get; set; }
        public string PostAuthorEchoId { get; set; }

        public virtual ICollection<SocialFeedAttachment> SocialFeedAttachments { get; set; }
    }
}
