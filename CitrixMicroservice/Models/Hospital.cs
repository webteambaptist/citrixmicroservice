﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class Hospital
    {
        public Hospital()
        {
            Departments = new HashSet<Department>();
        }

        public int Id { get; set; }
        public string Facility { get; set; }

        public virtual ICollection<Department> Departments { get; set; }
    }
}
