﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class PhysicianInfoHub
    {
        public int Id { get; set; }
        public string Echoid { get; set; }
        public DateTime ReadDate { get; set; }
        public string ContentId { get; set; }
    }
}
