﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class PhysicianQuicklink
    {
        public int Id { get; set; }
        public string Echoid { get; set; }
        public string LinkTitle { get; set; }
        public string LinkUrl { get; set; }
    }
}
