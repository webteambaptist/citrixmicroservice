﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class GeneralPhysicianInfo
    {
        public int Id { get; set; }
        public string Echoid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Credentials { get; set; }
    }
}
