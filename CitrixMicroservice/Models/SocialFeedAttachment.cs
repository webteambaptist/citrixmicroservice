﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class SocialFeedAttachment
    {
        public int Id { get; set; }
        public DateTime CreateDt { get; set; }
        public int PostId { get; set; }
        public byte[] PostAttachment { get; set; }
        public string PostFileName { get; set; }

        public virtual SocialFeedMain Post { get; set; }
    }
}
