﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CitrixMicroservice.Models
{
    public class ResourceModel
    {
        public string SessionID { get; set; } //asp.net_sessionid
        public string AuthID { get; set; } //CtxsAuthId
        public string CsrfToken { get; set; }
        public string AppID { get; set; } //same as launchURL sometimes
        public string LaunchURL { get; set; }
    }
}
