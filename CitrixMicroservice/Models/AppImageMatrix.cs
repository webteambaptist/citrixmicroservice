﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CitrixMicroservice.Models
{
    public partial class AppImageMatrix
    {
        public int Id { get; set; }
        public string AppName { get; set; }
        public int? ImageId { get; set; }
        public int AppOrder { get; set; }
    }
}
